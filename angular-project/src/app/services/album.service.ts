import { Injectable } from '@angular/core';
import { Album } from "../models/Album";
import { Observable, of } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AlbumService {

  albums: Album[];

  data: Observable<any>

  constructor() {
    this.albums = [{
      title: 'Inception',
      artist: 'Teminite',
      songs: ['Inception', 'Stormbringer', 'Earthquake', 'Spider VIP',
        'Beastmode', 'Get Down', 'Night Drive', 'Aspiration', 'Step Into The Light',
        'Everytime I Look Into The Sky'],
      favorite: 'Sounds really good',
      year: new Date('06/03/2016'),
      genre: 'Electronic',
      units: 0,
      img: '../../assets/imgs/inception.jpg',
      hide: false
    },
      {
        title: 'Different World',
        artist: 'Alan Walker',
        songs: ['Intro', 'Lost Control', "I Don't Wanna Go", 'Lily',
          'Lonely', 'Do It All for You', 'Different World', 'Interlude',
          'Sing Me to Sleep', 'All Falls Down', 'Darkside', 'Alone',
          'Diamond Heart', 'Faded (Interlude)', 'Faded'],
        favorite: 'great study album to listen to',
        year: new Date('2018'),
        genre: 'Electronic',
        units: 120,
        img: '../../assets/imgs/different-world.jpg',
        hide: false
      },
      {
        title: 'Uprising',
        artist: 'Teminite',
        songs: ['Uprising', 'State Of Mind', 'A New Dawn', 'Party Time',
          'Standing Tall', 'Make Me', 'Rattlesnake', 'Monster', 'Crushing On You',
          'Ascent'],
        favorite: 'what got me started in listening to electronic',
        year: new Date('11/16/2018'),
        genre: 'Electronic',
        units: 0,
        img: '../../assets/imgs/uprising.jpg',
        hide: false
      },
      {
        title: "Noma's 1st Album",
        artist: 'Noma',
        songs: ['House Groove (Remix)', 'Air Walker (Remix)', 'Elecat',
          'Colda (Remix)', 'Louder Machine', 'Dune (Remix)', 'Blue Fog (Extended Remix)',
          'Piano Dance', 'Massive', 'Puzzle And Dragons (Remix)', 'Q',
          'Louder_Voices', 'Stutter Practice', "Kirby's AirRide (High Speed Medley)",
          'Brain Power (Long Version)'],
        favorite: 'found it and it sounded really nice',
        year: new Date('08/26/2014'),
        genre: 'Electronic',
        units: 1,
        img: '../../assets/imgs/noma-first-album.jpg',
        hide: false
      },
      {
        title: 'Firepower',
        artist: 'Teminite',
        songs: ['Evolution', 'Energize', 'Fireflies', 'Are You Ready',
          'Shockwave', 'Firepower', 'Cecil Pavlova', 'Elevate', 'Animal',
          "Goin' In", 'Never Give Up'],
        favorite: 'this is great for listening for when you need motivation',
        year: new Date('09/10/2017'),
        genre: 'Electronic',
        units: 0,
        img: '../../assets/imgs/firepower.jpg',
        hide: false
      }]
  }

  getAlbums(): Observable<Album[]> {
    console.log('Getting characters from dataService')
    return of(this.albums);
  }

  addAlbum(album: Album) {
    this.albums.unshift(album)
  }

  getData() {
    this.data = new Observable(observer => {
      setTimeout(() => {
        observer.next(1);
      }, 1000);

      setTimeout(() => {
        observer.next(2);
      }, 2000);

      setTimeout(() => {
        observer.next(3);
      }, 3000)

      setTimeout(() => {
        observer.next(4);
      }, 4000)
    });
    return this.data
  }
}
