export interface Album {
  title: string,
  artist: string,
  songs: string[],
  favorite: string,
  year: any,
  genre: string,
  units: number,
  cover?: string,
  img?: string,
  hide?: boolean,
}
