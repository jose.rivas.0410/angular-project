import {Component, OnInit, ViewChild} from '@angular/core';
import { Album } from "../../models/Album";
import { AlbumService } from "../../services/album.service";

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.css']
})
export class AlbumsComponent implements OnInit {

  album: Album = {
    title: '',
    artist: '',
    songs: [''],
    favorite: '',
    year: '',
    genre: '',
    units: null,
  }


  albums: Album[];
  showForm: boolean;
  currentStyle: {};

  data: any;

  @ViewChild('albumForm')form: any;

  constructor(private albumService: AlbumService) { }

  ngOnInit(): void {
    // this.albums = [{
    //   title: 'Inception',
    //   artist: 'Teminite',
    //   songs: ['Inception', 'Stormbringer', 'Earthquake', 'Spider VIP',
    //     'Beastmode', 'Get Down', 'Night Drive', 'Aspiration', 'Step Into The Light',
    //     'Everytime I Look Into The Sky'],
    //   favorite: '',
    //   year: new Date('06/03/2016'),
    //   genre: 'Electronic',
    //   units: 0,
    //   img: '../../assets/imgs/inception.jpg',
    //   hide: false
    // },
    //   {
    //     title: 'Different World',
    //     artist: 'Alan Walker',
    //     songs: ['Intro', 'Lost Control', "I Don't Wanna Go", 'Lily',
    //       'Lonely', 'Do It All for You', 'Different World', 'Interlude',
    //       'Sing Me to Sleep', 'All Falls Down', 'Darkside', 'Alone',
    //       'Diamond Heart', 'Faded (Interlude)', 'Faded'],
    //     favorite: '',
    //     year: new Date('2018'),
    //     genre: 'Electronic',
    //     units: 120,
    //     img: '../../assets/imgs/different-world.jpg',
    //     hide: false
    //   },
    //   {
    //     title: 'Uprising',
    //     artist: 'Teminite',
    //     songs: ['Uprising', 'State Of Mind', 'A New Dawn', 'Party Time',
    //       'Standing Tall', 'Make Me', 'Rattlesnake', 'Monster', 'Crushing On You',
    //       'Ascent'],
    //     favorite: '',
    //     year: new Date('11/16/2018'),
    //     genre: 'Electronic',
    //     units: 0,
    //     img: '../../assets/imgs/uprising.jpg',
    //     hide: false
    //   },
    //   {
    //     title: "Noma's 1st Album",
    //     artist: 'Noma',
    //     songs: ['House Groove (Remix)', 'Air Walker (Remix)', 'Elecat',
    //       'Colda (Remix)', 'Louder Machine', 'Dune (Remix)', 'Blue Fog (Extended Remix)',
    //       'Piano Dance', 'Massive', 'Puzzle And Dragons (Remix)', 'Q',
    //       'Louder_Voices', 'Stutter Practice', "Kirby's AirRide (High Speed Medley)",
    //       'Brain Power (Long Version)'],
    //     favorite: '',
    //     year: new Date('08/26/2014'),
    //     genre: 'Electronic',
    //     units: 1,
    //     img: '../../assets/imgs/noma-first-album.jpg',
    //     hide: false
    //   },
    //   {
    //     title: 'Firepower',
    //     artist: 'Teminite',
    //     songs: ['Evolution', 'Energize', 'Fireflies', 'Are You Ready',
    //       'Shockwave', 'Firepower', 'Cecil Pavlova', 'Elevate', 'Animal',
    //       "Goin' In", 'Never Give Up'],
    //     favorite: '',
    //     year: new Date('09/10/2017'),
    //     genre: 'Electronic',
    //     units: 0,
    //     img: '../../assets/imgs/firepower.jpg',
    //     hide: false
    //   }]

    this.setCurrentStyle()


    this.albumService.getAlbums().subscribe(album => {
      this.albums = album;
    })

    this.albumService.getData().subscribe(data => {
      console.log(data)
    })




  }

  toggleInfo(album) {
    album.hide = !album.hide
  }

  // addAlbum() {
  //   // what we want to do?
  //   this.albums.unshift(this.album);
  //
  //   // clear out the form / reset the form
  //   this.album = {
  //     title: '',
  //     artist: '',
  //     songs: [''],
  //     favorite: '',
  //     year: new Date(),
  //     genre: '',
  //     units: null
  //   }
  // }

  toggleForm() {
    this.showForm = !this.showForm
    console.log('toggle button clicked')
  }

  setCurrentStyle() {
    this.currentStyle ={
      'text-decoration': 'underline'
    }
  }

  onSubmit({value, valid}: {value: Album, valid: boolean}) {
    if (!valid) {
      console.log('Form is not valid')
    }
    else {
      value.hide = false;
    }

    this.albumService.addAlbum(value);
    this.form.reset();
  }

}
